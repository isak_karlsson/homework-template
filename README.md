# README #

Homework Latex Template

## Configurable ##

* "Name" defines the student name, eg: \Name{Isak Karlsson} will print "Isak Karlsson" where appropriate.
* "StudentID" defines the student id, eg: \StudentID{110011} will print "110011" where appropriate.
* "Session" defines the class term/session, eg: \Session{Spring, 2016} will print "Spring, 2016" where appropriate.
* "ClassName" defines the class name, eg: \ClassName{Data Structs} will print "Data Structs" where appropriate.
* "ClassNum" defines the class number, eg: \ClassNum{CS 47B} will print "CS 47B" where appropriate.
* "Homework" defines the homework number, eg: \Homework{5} will print "Homework 5" where appropriate.
* "DueDate" defines the due date, eg: \DueDate{January 1st} will print "January 1st" where appropriate.
* Margins right below parameters for easy editing.

## Additions ##

* \probnum will print out "Problem x" where x auto-increments using section-numbering.
* \partnum will print out "Part x.y" where x.y is part y of problem x
* \partlet will print using lettering, eg: 2.a (part a of problem 2)
* \partrom will print using roman numeral numbering, eg: 1.i (part i of problem 1)
* \pic{image.png}{5cm} will render image.png with 5cm with (centered), it will look in a subfolder named 'figs' for pictures as well, for cleaner organization.
* \begin{code} this is pseudo code \end{code} will create a pseudo code block. Math Latex sequences allowed, so you can use $x \leftarrow x + 1$ to denote iteration if you'd like.
* \rendercode{test.py} will render code from a file to the page.
* \floor{arg}, and \ceil{arg} will render floor and ceiling math delimiters around it's argument.


## Tips ##
* Download and use Sumatra PDF, since it will automatically reload a pdf after latex finishes compiling.
* Use "latexmk -pvc -pdf -interaction=nonstopmode" to compile your pdf while editing. 
    * "-pvc" will perform a "continuous" compile, meaning everytime it detects an edit in your work it will recompile the tex.
    * "-pdf" will compile your work to a pdf file
    * "-interaction=nonstopmode" will tell the compiler to ignore errors, and continue compiling as best as it can.
